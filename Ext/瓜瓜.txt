{
  "sites": [
    {
      "key": "2345_spider",
      "name": "2345影视(官源)",
      "type": 3,
      "api": "csp_YS2345",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/2345.json"
    },
    {
      "key": "sogou_spider",
      "name": "搜狗影视(官源)",
      "type": 3,
      "api": "csp_YSSogou",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/sogou.json"
    },
    {
      "key": "rrm_spider",
      "name": "人人迷(免解)",
      "type": 3,
      "api": "csp_Rrm",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "B9C8093A210EADA1"
    },
    {
      "key": "ikan_spider",
      "name": "爱看(SP)",
      "type": 3,
      "api": "csp_IKan",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "A9545C889B5D"
    },
    {
      "key": "360_spider",
      "name": "360影视(官源)",
      "type": 3,
      "api": "csp_YS360",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/360.json"
    },
    {
      "key": "nfzjm_spider",
      "name": "南府追剧(AT)",
      "type": 3,
      "api": "csp_Nfzjm",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
    {
      "key": "ysgc_spider",
      "name": "影视工厂(V0)",
      "type": 3,
      "api": "csp_Ysgc",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
    {
      "key": "zjdr_spider",
      "name": "追剧达人(V0)",
      "type": 3,
      "api": "csp_Zjdr",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
    {
      "key": "auete_spider",
      "name": "Auete影视(SP)",
      "type": 3,
      "api": "csp_Auete",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/auete.json"
    },
    {
      "key": "n0ys_spider",
      "name": "90影视(SP)",
      "type": 3,
      "api": "csp_N0ys",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/90ys.json"
    },
    {
      "key": "cokemv_spider",
      "name": "Cokemv(SP)",
      "type": 3,
      "api": "csp_Cokemv",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/cokemv.json"
    },
    {
      "key": "adys_spider",
      "name": "爱迪影视(SP)",
      "type": 3,
      "api": "csp_Aidi",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/aidi.json"
    },
    {
      "key": "enlienli_spider",
      "name": "嗯哩嗯哩(SP)",
      "type": 3,
      "api": "csp_Enlienli",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
    {
      "key": "jpys_spider",
      "name": "极品影视(SP)",
      "type": 3,
      "api": "csp_Jpys",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/jipin.json"
    },
    {
      "key": "buka_spider",
      "name": "真不卡(SP)",
      "type": 3,
      "api": "csp_Buka",
      "searchable": 0,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/buka.json"
    },
    {
      "key": "bwl87_spider",
      "name": "阿房影视(SP)",
      "type": 3,
      "api": "csp_EPang",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/bwl87.json"
    },
    {
      "key": "ftys_spider",
      "name": "饭团影视(SP)",
      "type": 3,
      "api": "csp_Ftys",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/fantuan.json"
    },
    {
      "key": "9ekk_spider",
      "name": "9亿看看(SP)",
      "type": 3,
      "api": "csp_Nekk",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/ext/9ekk.json"
    },
    {
      "key": "csp_YydsAli1",
      "name": "YYDS阿里(SP)",
      "type": 3,
      "api": "csp_YydsAli1",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 0
    },
    {
      "key": "csp_xpath_cjt",
      "name": "CJT影视(XP)",
      "type": 3,
      "api": "csp_XPathMac",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/xpath/cjtys.json"
    },
    {
      "key": "csp_xpath_saohuotv",
      "name": "骚火电影(XP)",
      "type": 3,
      "api": "csp_XPath",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/xpath/saohuotv2.json"
    },
    {
      "key": "csp_xpath_subb",
      "name": "素白白影视(XP)",
      "type": 3,
      "api": "csp_XPathSubb",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/xpath/subaibai.json"
    },
    {
      "key": "csp_xpath_dm84",
      "name": "动漫巴士(XP)",
      "type": 3,
      "api": "csp_XPath",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/xpath/dm84.json"
    },
    {
      "key": "csp_xpath_age",
      "name": "AGE动漫(XP)",
      "type": 3,
      "api": "csp_XPathAge",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/pub/xpath/agefans.json"
    }
  ],
  "lives": [
    {
      "group": "NewTV",
      "channels": [
        {
          "name": "NewTV爱情喜剧",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-aiqingxj/n-aiqingxj"]
        },
        {
          "name": "NewTV潮妈辣婆",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-cmlapo/n-cmlapo"]
        },
        {
          "name": "NewTV动作电影",
          "urls": [
            "http://183.207.248.71:80/cntv/live1/n-dongzuody/n-dongzuody"
          ]
        },
        {
          "name": "NewTV古装剧场",
          "urls": [
            "http://183.207.248.71:80/cntv/live1/n-guzhuangjc/n-guzhuangjc"
          ]
        },
        {
          "name": "NewTV黑莓电竞之夜",
          "urls": ["http://183.207.248.71:80/cntv/live1/wmyx/wmyx"]
        },
        {
          "name": "NewTV黑莓电影",
          "urls": [
            "http://183.207.248.71:80/cntv/live1/HD-8000k-1080P-Supermovie/HD-8000k-1080P-Supermovie"
          ]
        },
        {
          "name": "NewTV黑莓动画",
          "urls": ["http://183.207.248.71:80/cntv/live1/donghuawg/donghuawg"]
        },
        {
          "name": "NewTV家庭剧场",
          "urls": [
            "http://183.207.248.71:80/cntv/live1/n-jiatingjc/n-jiatingjc"
          ]
        },
        {
          "name": "NewTV金牌综艺",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-saishijx/n-saishijx"]
        },
        {
          "name": "NewTV精品大剧",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-jdaju/n-jdaju"]
        },
        {
          "name": "NewTV精品体育",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-jtiyu/n-jtiyu"]
        },
        {
          "name": "NewTV精品综合",
          "urls": [
            "http://183.207.248.71:80/cntv/live1/n-mingxingdp/n-mingxingdp"
          ]
        },
        {
          "name": "NewTV军旅剧场",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-junlvjc/n-junlvjc"]
        },
        {
          "name": "NewTV军事评论",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-junshipl/n-junshipl"]
        },
        {
          "name": "NewTV农业致富",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-nongyezf/n-nongyezf"]
        },
        {
          "name": "NewTV热播精选",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-xiqumd/n-xiqumd"]
        },
        {
          "name": "NewTV中国功夫",
          "urls": ["http://183.207.248.71:80/cntv/live1/n-gzkongfu/n-gzkongfu"]
        }
      ]
    }
  ],
  "parses": [
    {
      "name": "Json并发",
      "type": 2,
      "url": "Parallel"
    },
    {
      "name": "瓜瓜优选",
      "type": 2,
      "url": "Preference"
    },
    {
      "name": "Json轮询",
      "type": 2,
      "url": "Sequence"
    },
    {
      "name": "瓜瓜免嗅",
      "type": 2,
      "url": "Cucumber"
    },
    {
      "name": "久久追剧",
      "type": 1,
      "url": "https://svip.renrenmi.cc:2222/api/?key=fI3YQ3vxF5uREfNpDU&url=",
      "t": 187,
      "i": "check at 2021-12-16 20:36:56"
    },
    {
      "name": "雪人影视",
      "type": 1,
      "url": "https://svip.renrenmi.cc:2222/api/?key=S77lACKG9VTwNq5Upv&url=",
      "t": 193,
      "i": "check at 2021-12-16 20:36:45"
    },
    {
      "name": "抹茶猪",
      "type": 1,
      "url": "https://api.parwix.com:4433/analysis/json/?uid=1752&my=fhlwyABGHJLNRX1489&url=",
      "t": 198,
      "i": "check at 2021-12-16 20:36:22"
    },
    {
      "name": "久久追剧2",
      "type": 1,
      "url": "https://json.pangujiexi.com:12345/json.php?url=",
      "t": 223,
      "i": "check at 2021-12-16 20:37:01"
    },
    {
      "name": "影阅阁3",
      "type": 1,
      "url": "https://svip.renrenmi.cc:2222/api/?key=82KErJogweliHPt7Yp&url=",
      "t": 237,
      "i": "check at 2021-12-16 20:38:05"
    },
    {
      "name": "Vip影院",
      "type": 1,
      "url": "https://api.zakkpa.com:8888/analysis/json/?uid=39&my=bcgkmntABFHILRX057&url=",
      "t": 256,
      "i": "check at 2021-12-16 20:36:39"
    },
    {
      "name": "雨果影视",
      "type": 1,
      "url": "http://103.40.246.5:6881/ijsonononxxti/?url=",
      "t": 262,
      "i": "check at 2021-12-16 20:36:37"
    },
    {
      "name": "南府影视",
      "type": 1,
      "url": "http://play.szbodankyy.com/goubi000/?url=",
      "t": 277,
      "i": "check at 2021-12-16 20:38:00"
    },
    {
      "name": "追剧吧4",
      "type": 1,
      "url": "http://47.100.138.210:91/home/api?type=ys&uid=3567682&key=adjmqrxCGJNOQTUWX8&url=",
      "t": 303,
      "i": "check at 2021-12-16 20:36:29"
    },
    {
      "name": "快云影音1",
      "type": 1,
      "url": "https://jhjx.ptygx.com/xttyjx.php/?url=",
      "t": 362,
      "i": "check at 2021-12-16 20:37:21"
    },
    {
      "name": "黄河影视",
      "type": 1,
      "url": "http://jx.ledu8.cn/api/?key=P8QSgO61p1MpHV2ALn&url=",
      "t": 398,
      "i": "check at 2021-12-16 20:36:19"
    },
    {
      "name": "筋斗云",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=1494542&key=ijmqvwxABEHILMOT48&url=",
      "t": 474,
      "i": "check at 2021-12-16 20:36:37"
    },
    {
      "name": "漫岛",
      "type": 1,
      "url": "http://47.100.138.210:91/home/api?type=ys&uid=19540&key=fjlmuwyAFHJLNRVXY3&url=",
      "t": 480,
      "i": "check at 2021-12-16 20:38:55"
    },
    {
      "name": "酷扑TV",
      "type": 1,
      "url": "http://www.yyiw.cn/json-2.php?uid=1196313&key=bjmorwzAHJLNRTX246&url=",
      "t": 491,
      "i": "check at 2021-12-16 20:38:28"
    },
    {
      "name": "段友影视",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=998036&key=afhlnuvzDJLMOX3689&url=",
      "t": 555,
      "i": "check at 2021-12-16 20:37:49"
    },
    {
      "name": "看剧吧",
      "type": 1,
      "url": "http://vip.mengx.vip/home/api?type=ys&uid=3714324&key=adhmprtwyzBCHLSUX1&url=",
      "t": 561,
      "i": "check at 2021-12-16 20:37:25"
    },
    {
      "name": "4K影院1",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=1095368&key=cdgjpsuDFLNRTUVY03&url=",
      "t": 583,
      "i": "check at 2021-12-16 20:37:19"
    },
    {
      "name": "蓝光视频",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=1931000&key=gktuvyzABEORSYZ135&url=",
      "t": 601,
      "i": "check at 2021-12-16 20:37:49"
    },
    {
      "name": "追剧吧",
      "type": 1,
      "url": "https://www.lengyue.app/hdsfajdkljlasjklsaj/?url=",
      "t": 614,
      "i": "check at 2021-12-16 20:36:20"
    },
    {
      "name": "播放呀",
      "type": 1,
      "url": "https://i.hexh.ink/home/api?type=ys&uid=1767561&key=bdelmsvwBDEHMNQXYZ&url=",
      "t": 644,
      "i": "check at 2021-12-16 20:38:23"
    },
    {
      "name": "林谷影视1",
      "type": 1,
      "url": "http://vip.lvdoui.cn/home/api?type=ys&uid=2828442&key=bcjlmovxyBEFJPT128&url=",
      "t": 646,
      "i": "check at 2021-12-16 20:38:16"
    },
    {
      "name": "饭后电影",
      "type": 1,
      "url": "http://47.100.138.210:91/home/api?type=ys&uid=1947441&key=eghijmopwyGQSTUW59&url=",
      "t": 674,
      "i": "check at 2021-12-16 20:36:36"
    },
    {
      "name": "快云影音",
      "type": 1,
      "url": "http://mxjx.ptygx.com/mxjson.php/?url=",
      "t": 685,
      "i": "check at 2021-12-16 20:37:20"
    },
    {
      "name": "可米影视",
      "type": 1,
      "url": "http://www.kmysw.vip/json/?url=",
      "t": 689,
      "i": "check at 2021-12-16 20:36:52"
    },
    {
      "name": "疯狂看",
      "type": 1,
      "url": "https://a.zhuijula.top/yun_api.php?url=",
      "t": 752,
      "i": "check at 2021-12-16 20:38:41"
    },
    {
      "name": "小极影视",
      "type": 1,
      "url": "http://119.29.145.154:9999/appjson/1.php?url=",
      "t": 774,
      "i": "check at 2021-12-16 20:36:56"
    },
    {
      "name": "影阅阁4",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=594615&key=bcehpqtxCEGKMT0248&url=",
      "t": 818,
      "i": "check at 2021-12-16 20:38:05"
    },
    {
      "name": "小强TV",
      "type": 1,
      "url": "https://api.m3u8.tv:5678/home/api?type=ys&uid=867242&key=bchmpswDGLOQTXZ168&url=",
      "t": 833,
      "i": "check at 2021-12-16 20:36:23"
    },
    {
      "name": "段友影视",
      "type": 1,
      "url": "http://1.12.218.245/1696263602.php/?url=",
      "t": 861,
      "i": "check at 2021-12-16 20:37:45"
    },
    {
      "name": "阿姨追剧2",
      "type": 1,
      "url": "https://cs.024zs.com:4433/api/?key=P3lAZ3QJlv6KBNNLHM&url=",
      "t": 935,
      "i": "check at 2021-12-16 20:38:32"
    },
    {
      "name": "南府影视2",
      "type": 1,
      "url": "http://47.100.138.210:91/home/api?type=ys&uid=349825&key=bekqtuBDKMNPQSXY24&url=",
      "t": 967,
      "i": "check at 2021-12-16 20:38:16"
    },
    {
      "name": "Preference",
      "type": 1,
      "url": "bl#久久追剧$$雪人影视$$抹茶猪$$影阅阁3$$南府影视$$Vip影院$$快云影音1$$追剧吧4$$漫岛$$雨果影视$$酷扑TV$$追剧吧$$播放呀$$筋斗云$$饭后电影$$4K影院1$$可米影视$$林谷影视1$$小极影视$$蓝光视频$$影阅阁4$$南府影视2$$小强TV$$阿姨追剧2||mg#抹茶猪$$Vip影院$$雨果影视$$雪人影视$$影阅阁3$$久久追剧$$南府影视$$追剧吧4$$快云影音1$$漫岛$$酷扑TV$$看剧吧$$追剧吧$$播放呀$$饭后电影$$林谷影视1$$可米影视$$快云影音$$小极影视$$疯狂看$$南府影视2$$黄河影视$$阿姨追剧2||pp#雪人影视$$影阅阁3$$抹茶猪$$久久追剧$$雨果影视$$Vip影院$$南府影视$$追剧吧4$$快云影音1$$酷扑TV$$漫岛$$看剧吧$$段友影视$$追剧吧$$播放呀$$饭后电影$$林谷影视1$$可米影视$$快云影音$$疯狂看$$小极影视$$阿姨追剧2$$南府影视2||qy#抹茶猪$$Vip影院$$雨果影视$$南府影视$$追剧吧4$$久久追剧$$雪人影视$$快云影音1$$影阅阁3$$酷扑TV$$久久追剧2$$黄河影视$$段友影视$$追剧吧$$4K影院1$$看剧吧$$蓝光视频$$播放呀$$饭后电影$$可米影视$$疯狂看$$小强TV$$影阅阁4$$小极影视$$南府影视2$$林谷影视1$$阿姨追剧2||1905#影阅阁3$$雪人影视$$久久追剧$$追剧吧4$$快云影音1$$漫岛$$段友影视$$看剧吧$$酷扑TV$$播放呀$$饭后电影$$可米影视$$快云影音$$小极影视$$南府影视2$$阿姨追剧2$$林谷影视1||yk#抹茶猪$$Vip影院$$影阅阁3$$雪人影视$$久久追剧$$雨果影视$$南府影视$$追剧吧4$$漫岛$$酷扑TV$$段友影视$$蓝光视频$$4K影院1$$追剧吧$$播放呀$$影阅阁4$$可米影视$$小极影视$$林谷影视1$$小强TV$$疯狂看$$南府影视2$$阿姨追剧2||sh#抹茶猪$$Vip影院$$雨果影视$$南府影视$$久久追剧2$$黄河影视$$追剧吧4$$快云影音1$$漫岛$$段友影视$$筋斗云$$4K影院1$$追剧吧$$饭后电影$$蓝光视频$$播放呀$$可米影视$$酷扑TV$$林谷影视1$$疯狂看$$小极影视$$南府影视2$$影阅阁4$$阿姨追剧2$$小强TV||cctv#影阅阁3$$久久追剧$$久久追剧2$$雪人影视$$追剧吧4$$雨果影视$$抹茶猪$$南府影视$$Vip影院$$黄河影视$$快云影音1$$酷扑TV$$看剧吧$$漫岛$$筋斗云$$追剧吧$$4K影院1$$饭后电影$$快云影音$$林谷影视1$$播放呀$$可米影视$$段友影视$$小极影视$$影阅阁4$$南府影视2$$阿姨追剧2$$小强TV$$蓝光视频||qq#久久追剧$$雪人影视$$雨果影视$$影阅阁3$$南府影视$$Vip影院$$追剧吧4$$快云影音1$$漫岛$$酷扑TV$$段友影视$$看剧吧$$黄河影视$$追剧吧$$久久追剧2$$林谷影视1$$饭后电影$$播放呀$$快云影音$$蓝光视频$$筋斗云$$疯狂看$$影阅阁4$$小强TV$$4K影院1$$南府影视2$$小极影视$$可米影视$$阿姨追剧2||mp#久久追剧2$$酷扑TV$$4K影院1$$筋斗云$$影阅阁4$$小强TV$$蓝光视频||le#追剧吧4$$快云影音1$$漫岛$$段友影视$$饭后电影$$播放呀$$可米影视$$小极影视$$阿姨追剧2$$南府影视2$$疯狂看||ac#追剧吧4$$快云影音1$$漫岛$$饭后电影$$播放呀$$可米影视$$小极影视$$阿姨追剧2$$南府影视2||migu#久久追剧2$$黄河影视$$筋斗云$$酷扑TV$$4K影院1$$蓝光视频$$林谷影视1$$影阅阁4$$小强TV$$阿姨追剧2"
    }
  ],
  "flags": [
    "youku",
    "qq",
    "iqiyi",
    "qiyi",
    "letv",
    "sohu",
    "tudou",
    "pptv",
    "mgtv",
    "wasu",
    "bilibili"
  ],
  "ijk": [
    {
      "group": "软解码",
      "options": [
        {
          "category": 4,
          "name": "opensles",
          "value": "0"
        },
        {
          "category": 4,
          "name": "overlay-format",
          "value": "842225234"
        },
        {
          "category": 4,
          "name": "framedrop",
          "value": "1"
        },
        {
          "category": 4,
          "name": "soundtouch",
          "value": "1"
        },
        {
          "category": 4,
          "name": "start-on-prepared",
          "value": "1"
        },
        {
          "category": 1,
          "name": "http-detect-range-support",
          "value": "0"
        },
        {
          "category": 1,
          "name": "fflags",
          "value": "fastseek"
        },
        {
          "category": 2,
          "name": "skip_loop_filter",
          "value": "48"
        },
        {
          "category": 4,
          "name": "reconnect",
          "value": "1"
        },
        {
          "category": 4,
          "name": "enable-accurate-seek",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-auto-rotate",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-handle-resolution-change",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-hevc",
          "value": "0"
        },
        {
          "category": 1,
          "name": "dns_cache_timeout",
          "value": "600000000"
        }
      ]
    },
    {
      "group": "硬解码",
      "options": [
        {
          "category": 4,
          "name": "opensles",
          "value": "0"
        },
        {
          "category": 4,
          "name": "overlay-format",
          "value": "842225234"
        },
        {
          "category": 4,
          "name": "framedrop",
          "value": "1"
        },
        {
          "category": 4,
          "name": "soundtouch",
          "value": "1"
        },
        {
          "category": 4,
          "name": "start-on-prepared",
          "value": "1"
        },
        {
          "category": 1,
          "name": "http-detect-range-support",
          "value": "0"
        },
        {
          "category": 1,
          "name": "fflags",
          "value": "fastseek"
        },
        {
          "category": 2,
          "name": "skip_loop_filter",
          "value": "48"
        },
        {
          "category": 4,
          "name": "reconnect",
          "value": "1"
        },
        {
          "category": 4,
          "name": "enable-accurate-seek",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-auto-rotate",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-handle-resolution-change",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-hevc",
          "value": "1"
        },
        {
          "category": 1,
          "name": "dns_cache_timeout",
          "value": "600000000"
        }
      ]
    }
  ],
  "ads": [
    "mimg.0c1q0l.cn",
    "www.googletagmanager.com",
    "www.google-analytics.com",
    "mc.usihnbcq.cn",
    "mg.g1mm3d.cn",
    "mscs.svaeuzh.cn",
    "cnzz.hhttm.top",
    "tp.vinuxhome.com",
    "cnzz.mmstat.com",
    "www.baihuillq.com",
    "s23.cnzz.com",
    "z3.cnzz.com",
    "c.cnzz.com",
    "stj.v1vo.top",
    "z12.cnzz.com",
    "img.mosflower.cn",
    "tips.gamevvip.com",
    "ehwe.yhdtns.com",
    "xdn.cqqc3.com",
    "www.jixunkyy.cn",
    "sp.chemacid.cn",
    "hm.baidu.com",
    "s9.cnzz.com",
    "z6.cnzz.com",
    "um.cavuc.com",
    "mav.mavuz.com",
    "wofwk.aoidf3.com",
    "z5.cnzz.com",
    "xc.hubeijieshikj.cn",
    "tj.tianwenhu.com",
    "xg.gars57.cn",
    "k.jinxiuzhilv.com",
    "cdn.bootcss.com",
    "ppl.xunzhuo123.com",
    "xomk.jiangjunmh.top",
    "img.xunzhuo123.com",
    "z1.cnzz.com",
    "s13.cnzz.com",
    "xg.huataisangao.cn",
    "z7.cnzz.com",
    "xg.huataisangao.cn",
    "z2.cnzz.com",
    "s96.cnzz.com",
    "q11.cnzz.com",
    "thy.dacedsfa.cn",
    "xg.whsbpw.cn",
    "s19.cnzz.com",
    "z8.cnzz.com",
    "s4.cnzz.com",
    "f5w.as12df.top",
    "ae01.alicdn.com",
    "www.92424.cn",
    "k.wudejia.com",
    "vivovip.mmszxc.top",
    "qiu.xixiqiu.com",
    "cdnjs.hnfenxun.com",
    "cms.qdwght.com"
  ],
  "spider": "https://litecucumber.coding.net/p/cat/d/config/git/raw/master/main_spider.txt;md5;d9c1641b4ff8345ebbef165208fe10c5"
}